# app.py

import json
from flask import Flask, request, make_response, jsonify
from sqlalchemy import create_engine

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://admin:B5F4soft!@chatbot.ctmtolk7tqv8.us-west-2.rds.amazonaws.com/sales'

engine = create_engine('mysql://admin:B5F4soft!@chatbot.ctmtolk7tqv8.us-west-2.rds.amazonaws.com/sales', echo=True)

log = app.logger

@app.route('/')
def default():
    return 'No method specified'

@app.route('/', methods=['POST'])
def webhook():
    req = request.get_json(silent=True, force=True)
    
    try:
        text = req.get('queryResult').get('queryText')
    except AttributeError:
        return 'json error'

    if text == 'lambda':
        res = 'lambda works!'
    elif text == 'lambda today':
        res = 'lambda today works!'
    elif 'average' or 'sales' in text:
        t = engine.execute('select AVG(sales) from superstore').fetchall()
        res = 'The average sale made at our superstore is $' + str(round(t[0][0], 2)) + '.'
    else:
        res = "Sorry, I'm not smart enough to help you ._."
        
    return make_response(jsonify({'fulfillmentText': res}))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')